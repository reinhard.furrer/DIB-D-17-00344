#!/bin/bash
# init_order_par_noethn.R

Rout="Rout"
Rfiles="Rcodes"

echo

# Standard syntax.
for a in 3 4 5 6 7 8 9 10
do
  rm -f $Rfiles/"torun.r"
  echo "max.par <- $a" >> $Rfiles/torun.r
  cat $Rfiles/init_order_par_noethn.R >> $Rfiles/torun.r
  R CMD BATCH $Rfiles/torun.r $Rout/out_noethn$a.txt
  echo -n "$a" 
done  

rm -f $Rfiles/"torun.r"

echo; echo
