# Project Title

Data on Leptospira interrogans sv Pomona infection in Meat Workers in New Zealand

Data on a prospective cohort study conducted in four sheep slaughtering 
abattoirs in New Zealand. 

## Getting Started

The repository DIB-D-17-00344 contains three directories.
They are respectively called Data, Figures and Rcodes.

### Data

In the data directory there are the raw data, used for the analysis, together 
with the resulting from some steps from the analysis.

### Figures

In the figures folder are contained all the relevant figures resulting from 
the analysis and present in the related Data in Brief article.

### Rcodes

In the Rcodes directory there are the seventeen files used to analyze the data,
they are numbered following the order they need to be used.


## Contributing

Please read also the related paper related to this study "Comparison between 
Generalized Linear Modelling and Additive Bayesian Network; Identification of 
Factors associated with the Incidence of Antibodies against Leptospira 
interrogans sv Pomona in Meat Workers in New Zealand" for a deeper understanding: 
http://www.sciencedirect.com/science/article/pii/S0001706X16308828


## Authors

* **Marta Pittavino**: marta.pittavino@math.uzh.ch 
* **Reinhard Furrer**: reinhard.furrer@math.uzh.ch 





